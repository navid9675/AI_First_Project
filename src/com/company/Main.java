package com.company;


import java.awt.*;
import java.io.*;
import java.util.ArrayList;


class Console {
    private PrintStream outputStream;
    private static Console myInstance;

    /**
     * @param out : given output stream
     */
    private Console(PrintStream out) {
        this.outputStream = out;
    }

    /**
     * @param out : given output stream
     * @return Console instance as console
     */
    public static Console getInstance(PrintStream out) {
        if (Console.myInstance == null) {
            Console.myInstance = new Console(out);
        }
        return Console.myInstance;
    }

    /**
     * @param message
     */
    public void log(String message) {
        outputStream.println("info:");
        outputStream.print(message);
    }

    /**
     * @param message
     */
    public void err(String message) {
        outputStream.println("error:");
        outputStream.print(message);
    }
}

class Point {
    int rowNumber;
    int colNumber;

    Point(int row, int col) {
        this.rowNumber = row;
        this.colNumber = col;
    }

    @Override
    public String toString() {
        StringBuilder x = new StringBuilder();

        x.append("Point:\nRow#: ").
                append(rowNumber).
                append(", Col#: ").
                append(colNumber);

        return x.toString();
    }

}

class SingleThreatPoint {
    Point kingPoint;
    Point coordination;

    SingleThreatPoint(Point coordination, Point kingPoint) {
        this.coordination = coordination;
        this.kingPoint = kingPoint;
    }

    @Override
    public String toString() {
        StringBuilder x = new StringBuilder();

        x.append("Single Threaten Point:\nKingPoint: \n").
                append(kingPoint.toString()).
                append("\n").
                append("Threaten Point: ").
                append("\n").
                append(coordination.toString());

        return x.toString();
    }

}

class Game implements Serializable {
    int row, column;
    char[][] map;
    char[][] threatMap;
    ArrayList<SingleThreatPoint> singleThreatenPoints;

    Game(int row, int column, char[][] map) {
        this.column = column;
        this.row = row;
        this.map = map;
        this.threatMap = new char[row][column];

        this.singleThreatenPoints = new ArrayList<>();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                this.threatMap[i][j] = map[i][j];
            }
        }
    }

    private boolean isPointInside(int r, int c) {

        return !(r < 0 || r >= this.row || c < 0 || c >= this.column);
    }

    private void surroundingThreat(int i, int j) {

        /*
         * for each given pair of i,j which is shown by 'X'
         * surrounding region marking would be something like this:
         *
         *  +-----------+
         *  | 1 | 2 | 3 |
         *  | 4 | X | 5 |
         *  | 6 | 7 | 8 |
         *  +-----------+
         */

        for (int counter1 = -1; counter1 < 2; counter1++) {
            for (int counter2 = -1; counter2 < 2; counter2++) {

                if (counter1 == 0 && counter2 == 0) {
                    continue;
                }

                if (!isPointInside(i + counter1, j + counter2)) {
                    continue;
                }

                if (map[i + counter1][j + counter2] == 'P') {
                    continue;
                }

                threatMap[i + counter1][j + counter2]++;
            }
        }
    }

    private void calculateThreat() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (threatMap[i][j] == 'K') {
                    surroundingThreat(i, j);
                }
            }
        }
    }

    private ArrayList<Point> findSingleThreatenPoints() {
        ArrayList<Point> noThreatPoints = new ArrayList<>();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (threatMap[i][j] == '1') {
                    noThreatPoints.add(new Point(i, j));
                }
            }
        }

        return noThreatPoints;
    }

    private void assignSTPToKing(ArrayList<Point> stpList) {
        for (Point p : stpList) {
            int i = p.rowNumber;
            int j = p.colNumber;

            for (int counter1 = -1; counter1 < 2; counter1++) {
                for (int counter2 = -1; counter2 < 2; counter2++) {

                    if (!isPointInside(i + counter1, j + counter2)) {
                        continue;
                    }

                    if (threatMap[i + counter1][j + counter2] == 'K') {
                        this.singleThreatenPoints.add(new SingleThreatPoint(p, new Point(i + counter1, j + counter2)));
                    }
                }
            }

        }
    }

    public void processGameState() {
        this.calculateThreat();
        ArrayList<Point> temp = this.findSingleThreatenPoints();

        assignSTPToKing(temp);
    }


    //Util functions:
    private static final String DEFAULT_PATH = "./maps/";

    public static Game readGameFromFile(String path, String filename) {
        Game game = null;

        if (path.trim().equals("")) {
            path = DEFAULT_PATH;
        }

        try {
            FileInputStream fileIn = new FileInputStream(path + filename);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            game = (Game) objectIn.readObject();

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        return game;
    }

    public static Game readGameFromFile(String filename) {

        return readGameFromFile(DEFAULT_PATH + "input/", filename);
    }

    public static void writeToGameFile(Game myGame, String path, String filename) {

        try {

            FileOutputStream fileOut = new FileOutputStream(path + filename);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(myGame);
            objectOut.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void writeToGameFile(Game myGame, String filename) {

        writeToGameFile(myGame, DEFAULT_PATH + "output/", filename);
    }

    @Override
    public String toString() {
        StringBuilder x = new StringBuilder();
        x.append("row:").append(this.row).append(", column:").append(this.column).append("\n");

        for (int i = 0; i < row; i++) {
            x.append("|");
            for (int j = 0; j < column; j++) {
                x.append(" ").append(this.map[i][j]);
            }
            x.append(" |\n");
        }

        x.append("Thread map:\n");

        for (int i = 0; i < row; i++) {
            x.append("|");
            for (int j = 0; j < column; j++) {
                x.append(" ").append(this.threatMap[i][j]);
            }
            x.append(" |\n");
        }

        x.append("single Threaten Points:\n");

        for (SingleThreatPoint stp : this.singleThreatenPoints) {
            x.append(stp.toString());
        }

        return x.toString();
    }
}

public class Main {


    public static void main(String[] args) {
        Console console = Console.getInstance(System.out);

        char[][] testMap = {
                {'K', '0', '0'},
                {'0', '0', 'K'},
                {'K', '0', 'P'}
        };
        Game x = new Game(3, 3, testMap);
        x.processGameState();

        console.log(x.toString());

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static void getAllFiles() {
        File curDir = new File(".");
        Console console = Console.getInstance(System.out);
        File[] filesList = curDir.listFiles();
        for (File f : filesList) {
            console.log(f.getName());
        }
    }
}
